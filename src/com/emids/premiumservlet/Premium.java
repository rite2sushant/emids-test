package com.emids.premiumservlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.emids.premiumcaculator.CalculateAmount;

/**
 * Servlet implementation class Premium
 */
@WebServlet("/Premium")
public class Premium extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public Premium() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
			}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String name = request.getParameter("name");
		int age = Integer.parseInt(request.getParameter("ageinput"));
		String gender=request.getParameter("gender");
		String hypertension=request.getParameter("hypertension");
		String bloodpressure=request.getParameter("bloodpressure");
		String sugar=request.getParameter("sugar");
		String overweight=request.getParameter("overweight");
		String smoking=request.getParameter("smoking");
		String alcohol=request.getParameter("alcohol");
		String excercise=request.getParameter("excercise");
		String drugs=request.getParameter("drugs");
		CalculateAmount  ca=new CalculateAmount();
		double premium=ca.calculateAmount(age, gender, hypertension, bloodpressure, sugar, overweight, smoking, alcohol, excercise, drugs);
		
		PrintWriter writer = response.getWriter();
		String htmlRespone = "<html>";
		htmlRespone += "<h2>"
				+ "Hii   " + name +"</h2>"
				+ "<h1>"+"Your Premium amount is: " + Math.ceil(premium) + "</h1>";
		htmlRespone += "</html>";
		 
		writer.println(htmlRespone);
		
		
	}

}
