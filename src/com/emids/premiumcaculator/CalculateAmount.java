package com.emids.premiumcaculator;

public class CalculateAmount {
	
	 double premium;
	public  double calculateAmount(int age,String gen,String ht,String bp,String sg,String ov,String sm,String alc,String de,String drg){
		
				premium=5000;
				ageCond(age);
				genderCheck(gen);
				healthCond(ht,bp,sg,ov);
				habitCond(sm,alc,de,drg);						
		return premium;
	}
	
	public  double ageCond(int age){
		
	
		if(age>=18 && age<25){
			premium=premium*1.1;
		}
		else if(age>=25 && age<30){
			premium=premium*1.21;
		}
		else if(age>=30 && age<35){
			premium=premium*1.331;
		}
		else if(age>=35 && age<40){
			premium=premium*1.461;
		}
		else if(age>=40){
			premium=premium*1.461;
			while(age>40){
				premium=premium*1.20;
				age=age-5;
			}
		}
			
		return premium;
	}
	
	
	public  void genderCheck(String gen){
		if(gen.equals("1")) premium=premium*1.02;
		
	}
	
	public  void healthCond(String ht,String bp,String sg,String ov){
		double bal=0;
		if(ht.equals("1")) bal++;
		if(bp.equals("1")) bal++;
		if(sg.equals("1")) bal++;
		if(ov.equals("1")) bal++;
		
		 premium=premium*(1+(bal/100));
	}
	public  void habitCond(String sm,String alc,String de,String drg){
		double bal=0;
		if(sm.equals("1")) bal++;
		if(alc.equals("1")) bal++;
		if(de.equals("1")) bal--;
		if(drg.equals("1")) bal++;
		bal=bal*3;
		 premium=premium*(1+(bal/100));
	}
	
}
